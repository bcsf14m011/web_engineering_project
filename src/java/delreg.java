import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.*;
import com.mysql.jdbc.Driver;
import java.sql.DriverManager;


public class delreg extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String site = new String("admin-home.jsp");

        try {
           
            String rnum = request.getParameter("regnum");
                        
            String create="com.mysql.jdbc.Driver";
            Class.forName(create);
            String url = "jdbc:mysql://localhost/motor_database";			
            String rn = "root";
            String ps = "";

            Connection con=DriverManager.getConnection(url,rn,ps);
            String sql_query_delete = "delete from vehicle_registeration where registration_number=?";

            PreparedStatement psi = con.prepareStatement(sql_query_delete);

	    psi.setString(1,rnum);
            
	    psi.executeUpdate();
            
            con.close();
            
            response.setContentType("text/html");
            response.setStatus(response.SC_MOVED_TEMPORARILY);
            response.setHeader("Location", site);         
        
        }
        catch(SQLException se)
	{
            out.print(se);
   	}
   	catch(ClassNotFoundException cnfe)
   	{
            out.print(cnfe);
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    public String getServletInfo() {
        return "Short description";
    }

}
