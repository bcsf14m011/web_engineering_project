import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.*;
import com.mysql.jdbc.Driver;
import java.sql.DriverManager;



public class newreg extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String site = new String("admin-home.jsp");

        try {
           
            String rnum = request.getParameter("regnum");
            String cnum = request.getParameter("cnum");
            String enumb = request.getParameter("enum");
            String make = request.getParameter("make");
            String model = request.getParameter("model");
            String color = request.getParameter("color");
            int p = Integer.parseInt(request.getParameter("price"));
            String own = request.getParameter("ownname");
            String ownf = request.getParameter("ownf");
            String ownc = request.getParameter("ownc");       
            
            String create="com.mysql.jdbc.Driver";
            Class.forName(create);
            String url = "jdbc:mysql://localhost/motor_database";			
            String rn = "root";
            String ps = "";

            Connection con=DriverManager.getConnection(url,rn,ps);

            String sql_query_insert = "select * from vehicle_registeration";
						
            PreparedStatement psi = con.prepareStatement(sql_query_insert,ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            ResultSet rs = psi.executeQuery();

            rs.moveToInsertRow();
            rs.updateString(1,rnum);
            rs.updateString(2,cnum);
            rs.updateString(3,enumb);
            rs.updateString(4,make);
            rs.updateString(5,model);
            rs.updateString(6,color);
            rs.updateInt(7,p);
            rs.updateString(8,own);
            rs.updateString(9,ownf);
            rs.updateString(10,ownc);
            
            rs.insertRow();
            
            con.close();
            
            response.setContentType("text/html");
            response.setStatus(response.SC_MOVED_TEMPORARILY);
            response.setHeader("Location", site);         
        
        }
        catch(SQLException se)
	{
            out.print(se);
   	}
   	catch(ClassNotFoundException cnfe)
   	{
            out.print(cnfe);
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    public String getServletInfo() {
        return "Short description";
    }

}
