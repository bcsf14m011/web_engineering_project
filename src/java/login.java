import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.*;
import com.mysql.jdbc.Driver;
import java.sql.DriverManager;

public class login extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String siteas = "admin-home.jsp";
        String siteds = "dealer.jsp";
        String sitee = "login.jsp";
      
        try {
            String n = request.getParameter("uname");
            String p = request.getParameter("password");
            String t = request.getParameter("type");
            
            String create="com.mysql.jdbc.Driver";
            Class.forName(create);
            String url = "jdbc:mysql://localhost/motor_database";			
            String rn = "root";
            String ps = "";

            Connection con=DriverManager.getConnection(url,rn,ps);
		
            String sql_query_insert = "select * from login where Name='"+n+"'";
						
            PreparedStatement psi = con.prepareStatement(sql_query_insert);
            ResultSet rs = psi.executeQuery();
            
            
            if(rs.next())
            {
                String nn = rs.getString(1);
                String pp = rs.getString(2);
                String tt = rs.getString(3);
            
                con.close();
            
                if(n.equals(nn) && p.equals(pp) && t.equals(tt))
                {
                    HttpSession session = request.getSession();
                    session.setAttribute("type",t);
                    session.setAttribute("uname",n);
                
                    response.setContentType("text/html");
                    response.setStatus(response.SC_MOVED_TEMPORARILY);
                
                    if(t.equals("Admin"))
                    {
                        response.setHeader("Location", siteas);
                    }
                    if(t.equals("Dealer"))
                    {
                        response.setHeader("Location", siteds);
                    }       
                    
                    else
                    {
                        response.setContentType("text/html");
                        response.setStatus(response.SC_MOVED_TEMPORARILY);
                        response.setHeader("Location", sitee);
                    }
                }
                else
                {
                    response.setContentType("text/html");
                    response.setStatus(response.SC_MOVED_TEMPORARILY);
                    response.setHeader("Location", sitee);
                }
            }
            else
            {
                response.setContentType("text/html");
                response.setStatus(response.SC_MOVED_TEMPORARILY);
                response.setHeader("Location", sitee);
            }
        }
        catch(SQLException se)
	{
           
            out.print(se);
   	}
   	catch(ClassNotFoundException cnfe)
   	{
           out.print(cnfe);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
