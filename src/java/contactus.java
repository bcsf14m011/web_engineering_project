import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.*;
import com.mysql.jdbc.Driver;
import java.sql.DriverManager;

/**
 *
 * @author Shutter_Island
 */
public class contactus extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String site = "index.jsp";
        PrintWriter out = response.getWriter();
                
        try {
            
            String n = request.getParameter("name");
            String e = request.getParameter("email");
            String s = request.getParameter("subject");
            String m = request.getParameter("msg");       
            
            String create="com.mysql.jdbc.Driver";
            Class.forName(create);
            String url = "jdbc:mysql://localhost/motor_database";			
            String rn = "root";
            String ps = "";

            Connection con=DriverManager.getConnection(url,rn,ps);
		
            String sql_query_insert = "select * from contact_us";
						
            PreparedStatement psi = con.prepareStatement(sql_query_insert,ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            ResultSet rs = psi.executeQuery();

            rs.moveToInsertRow();
            rs.updateString(1,n);
            rs.updateString(2,e);
            rs.updateString(3,s);
            rs.updateString(4,m);
            rs.insertRow();
            
            con.close();
            
            response.setContentType("text/html");
            response.setStatus(response.SC_MOVED_TEMPORARILY);
            response.setHeader("Location", site); 
            
        }
        catch(SQLException se)
	{
           
            out.print(se);
   	}
   	catch(ClassNotFoundException cnfe)
   	{
           out.print(cnfe);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}