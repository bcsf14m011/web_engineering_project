<%@ page import="java.sql.*"%>
<% Class.forName("com.mysql.jdbc.Driver");%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE HTML>
<head>
<title>Vehicle Verification</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<link rel="stylesheet" href="homestyle.css" type="text/css" charset="utf-8" />
</head>
<body>

    <style>
        .ent-main-div {
            width: 40%;
            margin: auto;
            padding: 30px;
            background-color: #f5f5f5;
            border-radius: 10px;
        }
        .ent-info-div {
            font-size: 10px;
            margin: auto;
            width: 50%;
        }
        .ent-textbox-div {
            padding: 20px;
        }
        .ent-textbox-div span {
            font-weight: bold;
        }
        .ent-textbox-div input {
            width: 100%;
        }
        .ent-button-div {
            padding-right: 20px;
            text-align: right;
        }
        @media screen and (max-width: 767px){
            .ent-main-div {
                width: 100%; /*100*/
                margin: auto;
                padding: 15px; /*15*/
                background-color: #f5f5f5;
                border-radius: 10px;
            }
            .ent-info-div {
                font-size: 10px;
                margin: auto;
                width: 70%;
                text-align: center;
            }
        }
    </style>


    
<%!
            public class connect  
            {      
                String url = "jdbc:mysql://localhost/motor_database";			
                String rn = "root";
                String ps = "";
                String sql_query_insert = "select * from vehicle_registeration where registration_number=?";
                String vnum;

                Connection con = null;
                PreparedStatement psi = null;
                ResultSet rs = null;
                
                public connect()
                {
                    try
                    {
                        con=DriverManager.getConnection(url,rn,ps);
                        psi = con.prepareStatement(sql_query_insert,ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);                    
                    }
                    catch(SQLException se)
                    {
                                
                    }
                }
                private void getVal(HttpServletRequest request) 
                {
                    vnum = request.getParameter("vhlno");
                }
                public ResultSet getDetails()
                {
                    try
                    {
                       psi.setString(1,vnum);
                       rs = psi.executeQuery(); 
                    }
                    catch(SQLException se)
                    {
                                
                    }

                    return rs;
                }    
            }
        %>

         <div class="region region-content">
                                    <section id="block-system-main" class="block block-system clearfix">
                                        <div id="node-6" class="node node-page clearfix" typeof="foaf:Document">
                                            <div class="ent-main-div">
                                            <center>
                                            <center><h3>Vehicle Details</h3></center>
                                            
        <%
            connect c = new connect();
            c.getVal(request);

            ResultSet details = c.getDetails();
            if (details.next()) 
            {
               %>
                                            
                                            <br><br>
                                              <table>
                                                  <tbody>
                                                <tr>
                                                    <td>Registration Number:</td>
                                                    <td><%= details.getString("registration_number")%></td>
                                                </tr>
                                                <tr>
                                                    <td>Chassis Number:</td>
                                                    <td><%= details.getString("chassis_number") %></td>
                                                </tr>
                                                <tr>
                                                    <td>Engine Number:</td>
                                                    <td><%= details.getString("engine_number") %></td>
                                                </tr>
                                                <tr>
                                                    <td>Make Name:</td>
                                                    <td><%= details.getString("make") %></td>
                                                </tr>
                                                <tr>
                                                    <td>Model:</td>
                                                    <td><%= details.getString("model") %></td>
                                                </tr>
                                                <tr>
                                                    <td>Vehicle Price:</td>
                                                    <td><%= details.getString("vehicle_price") %></td>
                                                </tr>
                                                <tr>
                                                    <td>Color:</td>
                                                    <td><%= details.getString("color") %></td>
                                                </tr>
                                               
                                                  </tbody>
                                              </table>
                                        <br><br>
                                        <center><h3>Owner Details</h3></center>
                                        <br><br>
                                            <table>
                                                <tr>
                                                    <td>Owner name:</td>
                                                    <td><%= details.getString("owner_name") %></td>
                                                </tr>
                                                <tr>
                                                    <td>Father Name:</td>
                                                    <td><%= details.getString("owner_father") %></td>
                                                </tr>
                                                <tr>
                                                    <td>Owner City:</td>
                                                    <td><%= details.getString("owner_city") %></td>
                                                </tr>
                                              </table>  
                                                
<%
    }
    else
    {
%> 
                                <br><br>
                                <h4>No Record Found</h4>

<%
    }
%>
                                                
                                            </center>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
