<%@ page import="java.io.*,java.util.*" %>

<%
    if(!(null == session.getAttribute("type")))
    {
        String type = null;
        type = (String)session.getAttribute("type");

        if(type.equals("Dealer"))
        {
%>

<!DOCTYPE html>
<head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Dealer</title>
        <link href='http://fonts.googleapis.com/css?family=Nunito:400,300' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="dealerstyle.css">
</head>

<body>
    <center><h1>New Registration</h1></center>

    <fieldset>

        <form role="form" action="newdealerreg" method="post">
            <center><h2>Vehicle Details</h2></center>
                <label>Registration Number</label>
                <input name="regnum" placeholder="LEA-17-333" style="width: 435px; height:30px;"/>
                <label>Chassis Number</label>
                <input name="cnum" placeholder="LJCPCBLCX11000237" style="width: 435px; height:30px;"/>
                <label>Engine Number</label>
                <input name="enum" placeholder="321PCBLCX11CPL237" style="width: 435px; height:30px;"/>
                <label>Make</label>
                    <select name="make">
                        <option value="Toyota">Toyota</option>
                        <option value="Honda">Honda</option>
                        <option value="Suzuki">Suzuki</option>
                        <option value="Daihatsu">Daihatsu</option>
                        <option value="Porsche">Porsche</option>
                        <option value="Ford">Ford</option>
                        <option value="Bentley">Bentley</option>
                        <option value="Other">Other</option>
                    </select>
                <label>Model</label>
                <input name="model" placeholder="1992 Corolla" style="width: 435px; height:30px;"/>
                <label>Color</label>
                <select name="color">
                        <option value="White">White</option>
                        <option value="Black">Black</option>
                        <option value="Silver">Silver</option>
                        <option value="Charcol">Charcol</option>
                        <option value="Gold">Gold</option>
                        <option value="Blue">Blue</option>
                        <option value="Red">Red</option>
                        <option value="Green">Green</option>
                        <option value="Other">Other</option>
                    </select>
                <label>Vehicle Price</label>
                <input type="number" name="price" placeholder="3800000" style="width: 435px; height:30px;"/>
                <label>Owner Name</label>
                <input name="ownname" placeholder="Ahsan Malik" style="width: 435px; height:30px;"/>
                <label>Owner Father</label>
                <input name="ownf" placeholder="Talha Jahangir" style="width: 435px; height:30px;"/>
                <label>Owner City</label>
                <select name="ownc">
                        <option value="Lahore">Lahore</option>
                        <option value="Islamabad">Islamabad</option>
                        <option value="Faisalabad">Faisalabad</option>
                        <option value="Multan">Multan</option>
                        <option value="Bahawalpur">Bahawalpur</option>
                        <option value="Mianwali">Mianwali</option>
                        <option value="Sialkot">Sialkot</option>
                        <option value="DG Khan">Dera Ghazi Khan</option>
                    </select>
            <br/><br/>
            <button type="submit" class="btn btn-success pull-right btn-lg">Register Vehicle</button>

        </form>

    </fieldset>

      <form action="endsession" method="post">
        <button type="submit">Logout</button>
    </form>
    
  </body>
</html>

<%
        }
        else
        {
            response.sendRedirect("login.jsp");
        }
    }
    else
    {
        response.sendRedirect("login.jsp");
    }
%>