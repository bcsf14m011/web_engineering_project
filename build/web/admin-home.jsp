<%@ page import="java.sql.*"%>
<% Class.forName("com.mysql.jdbc.Driver");%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%
    if(!(null == session.getAttribute("type")))
    {
        String type = null;
        type = (String)session.getAttribute("type");

        if(type.equals("Admin"))
        {
%>

<!DOCTYPE html>
<head>
      <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Admin</title>

    <link href="adminstyle.css" rel="stylesheet" />
    
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
</head>
<body>

<%!
            public class connect
            {
                String url = "jdbc:mysql://localhost/motor_database";			
                String rn = "root";
                String ps = "";
                String sql_query_insert = "select * from vehicle_registeration";
                
                Connection con = null;
                PreparedStatement psi = null;
                ResultSet rs = null;
                
                public connect()
                {
                    try
                    {
                        con=DriverManager.getConnection(url,rn,ps);
                        //Statement s = con.createStatement();			
                        psi = con.prepareStatement(sql_query_insert,ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);                    
                    }
                    catch(SQLException se)
                    {
                                
                    }
                }
                public ResultSet getdetails()
                {
                    try
                    {
                       rs = psi.executeQuery(); 
                    }
                    catch(SQLException se)
                    {
                                
                    }

                return rs;
                }    
            }
        %>

        <%
            connect c = new connect();
            ResultSet details = c.getdetails();
        %>


    <div id="wrapper">
        <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="admin-home.jsp">Registeration Admin</a> 
            </div>
  <div style="color: white;
padding: 15px 50px 5px 50px;
float: right;
font-size: 16px;"><a href="endsession" class="btn btn-danger square-btn-adjust">Logout</a> </div>
        </nav>   

                <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">
				<li class="text-center">
                    <img src="images/find_user.png" class="user-image img-responsive"/>
					</li>
				
					
                    <li>
                        <a class="active-menu"  href="admin-home.jsp"><i class="fa fa-dashboard fa-3x"></i> Home</a>
                    </li>
                     <li>
                        <a  href="new-reg.jsp"><i class="fa fa-desktop fa-3x"></i> New Registeration</a>
                    </li>
                    <li>
                        <a  href="change-own.jsp"><i class="fa fa-qrcode fa-3x"></i> Change Ownership</a>
                    </li>
						   <li  >
                        <a   href="update-eng-num.jsp"><i class="fa fa-bar-chart-o fa-3x"></i> Update Engine Number</a>
                    </li>	
                      <li  >
                        <a  href="del-reg.jsp"><i class="fa fa-table fa-3x"></i> Delete Registeration</a>
                    </li>
                    
                </ul>
               
            </div>
            
        </nav>  

        <div id="page-wrapper" >
           <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                     <h2>Admin Dashboard</h2>
                    </div>
                </div>
                 <hr />
               
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                             Registered Vehicles
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>Registration Number</th>
                                            <th>Owner Name</th>
                                            <th>Owner Father</th>
                                            <th>Owner City</th>
                                            <th>Make</th>
                                            <th>Model</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                       <% while(details.next()) { %>
                
                                        <tr>
                                        <td><%= details.getString("registration_number") %></td>
                                        <td><%= details.getString("owner_name") %></td>
                                        <td><%= details.getString("owner_father") %></td>
                                        <td><%= details.getString("owner_city") %></td>
                                        <td><%= details.getString("make") %></td>
                                        <td><%= details.getString("model") %></td>                  						
                                        </tr>
                			<% } %>
                                    </tbody>
                                   
                                </table>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>        
  
    	</div>
   
</body>
</html>

<%
        }
        else
        {
            response.sendRedirect("login.jsp");
        }
    }
    else
    {
        response.sendRedirect("login.jsp");
    }
%>