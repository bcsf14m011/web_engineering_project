package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.io.*;
import java.util.*;

public final class dealer_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write('\n');
      out.write('\n');

    if(!(null == session.getAttribute("type")))
    {
        String type = null;
        type = (String)session.getAttribute("type");

        if(type.equals("Dealer"))
        {

      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<head>\n");
      out.write("        <meta charset=\"utf-8\">\n");
      out.write("        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n");
      out.write("        <title>Dealer</title>\n");
      out.write("        <link href='http://fonts.googleapis.com/css?family=Nunito:400,300' rel='stylesheet' type='text/css'>\n");
      out.write("        <link rel=\"stylesheet\" href=\"dealerstyle.css\">\n");
      out.write("</head>\n");
      out.write("\n");
      out.write("<body>\n");
      out.write("    <center><h1>New Registration</h1></center>\n");
      out.write("\n");
      out.write("    <fieldset>\n");
      out.write("\n");
      out.write("        <form role=\"form\" action=\"newdealerreg\" method=\"post\">\n");
      out.write("            <center><h2>Vehicle Details</h2></center>\n");
      out.write("                <label>Registration Number</label>\n");
      out.write("                <input name=\"regnum\" placeholder=\"LEA-17-333\" style=\"width: 435px; height:30px;\"/>\n");
      out.write("                <label>Chassis Number</label>\n");
      out.write("                <input name=\"cnum\" placeholder=\"LJCPCBLCX11000237\" style=\"width: 435px; height:30px;\"/>\n");
      out.write("                <label>Engine Number</label>\n");
      out.write("                <input name=\"enum\" placeholder=\"321PCBLCX11CPL237\" style=\"width: 435px; height:30px;\"/>\n");
      out.write("                <label>Make</label>\n");
      out.write("                    <select name=\"make\">\n");
      out.write("                        <option value=\"Toyota\">Toyota</option>\n");
      out.write("                        <option value=\"Honda\">Honda</option>\n");
      out.write("                        <option value=\"Suzuki\">Suzuki</option>\n");
      out.write("                        <option value=\"Daihatsu\">Daihatsu</option>\n");
      out.write("                        <option value=\"Porsche\">Porsche</option>\n");
      out.write("                        <option value=\"Ford\">Ford</option>\n");
      out.write("                        <option value=\"Bentley\">Bentley</option>\n");
      out.write("                        <option value=\"Other\">Other</option>\n");
      out.write("                    </select>\n");
      out.write("                <label>Model</label>\n");
      out.write("                <input name=\"model\" placeholder=\"1992 Corolla\" style=\"width: 435px; height:30px;\"/>\n");
      out.write("                <label>Color</label>\n");
      out.write("                <select name=\"color\">\n");
      out.write("                        <option value=\"White\">White</option>\n");
      out.write("                        <option value=\"Black\">Black</option>\n");
      out.write("                        <option value=\"Silver\">Silver</option>\n");
      out.write("                        <option value=\"Charcol\">Charcol</option>\n");
      out.write("                        <option value=\"Gold\">Gold</option>\n");
      out.write("                        <option value=\"Blue\">Blue</option>\n");
      out.write("                        <option value=\"Red\">Red</option>\n");
      out.write("                        <option value=\"Green\">Green</option>\n");
      out.write("                        <option value=\"Other\">Other</option>\n");
      out.write("                    </select>\n");
      out.write("                <label>Vehicle Price</label>\n");
      out.write("                <input type=\"number\" name=\"price\" placeholder=\"3800000\" style=\"width: 435px; height:30px;\"/>\n");
      out.write("                <label>Owner Name</label>\n");
      out.write("                <input name=\"ownname\" placeholder=\"Ahsan Malik\" style=\"width: 435px; height:30px;\"/>\n");
      out.write("                <label>Owner Father</label>\n");
      out.write("                <input name=\"ownf\" placeholder=\"Talha Jahangir\" style=\"width: 435px; height:30px;\"/>\n");
      out.write("                <label>Owner City</label>\n");
      out.write("                <select name=\"ownc\">\n");
      out.write("                        <option value=\"Lahore\">Lahore</option>\n");
      out.write("                        <option value=\"Islamabad\">Islamabad</option>\n");
      out.write("                        <option value=\"Faisalabad\">Faisalabad</option>\n");
      out.write("                        <option value=\"Multan\">Multan</option>\n");
      out.write("                        <option value=\"Bahawalpur\">Bahawalpur</option>\n");
      out.write("                        <option value=\"Mianwali\">Mianwali</option>\n");
      out.write("                        <option value=\"Sialkot\">Sialkot</option>\n");
      out.write("                        <option value=\"DG Khan\">Dera Ghazi Khan</option>\n");
      out.write("                    </select>\n");
      out.write("            <br/><br/>\n");
      out.write("            <input type=\"submit\" class=\"btn btn-success pull-right btn-lg\" name=\"submitbutton\" onclick=\"{return confirmComplete();}\" value=\"Register\">\n");
      out.write("        </form>\n");
      out.write("\n");
      out.write("    </fieldset>\n");
      out.write("\n");
      out.write("      <form action=\"endsession\" method=\"post\">\n");
      out.write("        <button type=\"submit\">Logout</button>\n");
      out.write("    </form>\n");
      out.write("    \n");
      out.write("  </body>\n");
      out.write("</html>\n");
      out.write("\n");

        }
        else
        {
            response.sendRedirect("login.jsp");
        }
    }
    else
    {
        response.sendRedirect("login.jsp");
    }

    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
