package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.sql.*;

public final class view_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {


            public class connect  
            {      
                String url = "jdbc:mysql://localhost/motor_database";			
                String rn = "root";
                String ps = "";
                String sql_query_insert = "select * from vehicle_registeration where registration_number=?";
                String vnum;

                Connection con = null;
                PreparedStatement psi = null;
                ResultSet rs = null;
                
                public connect()
                {
                    try
                    {
                        con=DriverManager.getConnection(url,rn,ps);
                        psi = con.prepareStatement(sql_query_insert,ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);                    
                    }
                    catch(SQLException se)
                    {
                                
                    }
                }
                private void getVal(String param, HttpServletRequest request) 
                {
                    vnum = request.getParameter("vhlno");
                }
                public ResultSet getDetails()
                {
                    try
                    {
                       psi.setString(1,vnum);
                       rs = psi.executeQuery(); 
                    }
                    catch(SQLException se)
                    {
                                
                    }

                    return rs;
                }    
            }
        
  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write('\n');
 Class.forName("com.mysql.jdbc.Driver");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE HTML>\n");
      out.write("<head>\n");
      out.write("<title>Vehicle Verification</title>\n");
      out.write("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"/>\n");
      out.write("<link rel=\"stylesheet\" href=\"homestyle.css\" type=\"text/css\" charset=\"utf-8\" />\n");
      out.write("</head>\n");
      out.write("<body>\n");
      out.write("\n");
      out.write("    <style>\n");
      out.write("        .ent-main-div {\n");
      out.write("            width: 40%;\n");
      out.write("            margin: auto;\n");
      out.write("            padding: 30px;\n");
      out.write("            background-color: #f5f5f5;\n");
      out.write("            border-radius: 10px;\n");
      out.write("        }\n");
      out.write("        .ent-info-div {\n");
      out.write("            font-size: 10px;\n");
      out.write("            margin: auto;\n");
      out.write("            width: 50%;\n");
      out.write("        }\n");
      out.write("        .ent-textbox-div {\n");
      out.write("            padding: 20px;\n");
      out.write("        }\n");
      out.write("        .ent-textbox-div span {\n");
      out.write("            font-weight: bold;\n");
      out.write("        }\n");
      out.write("        .ent-textbox-div input {\n");
      out.write("            width: 100%;\n");
      out.write("        }\n");
      out.write("        .ent-button-div {\n");
      out.write("            padding-right: 20px;\n");
      out.write("            text-align: right;\n");
      out.write("        }\n");
      out.write("        @media screen and (max-width: 767px){\n");
      out.write("            .ent-main-div {\n");
      out.write("                width: 100%; /*100*/\n");
      out.write("                margin: auto;\n");
      out.write("                padding: 15px; /*15*/\n");
      out.write("                background-color: #f5f5f5;\n");
      out.write("                border-radius: 10px;\n");
      out.write("            }\n");
      out.write("            .ent-info-div {\n");
      out.write("                font-size: 10px;\n");
      out.write("                margin: auto;\n");
      out.write("                width: 70%;\n");
      out.write("                text-align: center;\n");
      out.write("            }\n");
      out.write("        }\n");
      out.write("    </style>\n");
      out.write("\n");
      out.write("\n");
      out.write("    \n");
      out.write("\n");
      out.write("\n");
      out.write("        \n");
      out.write("    <div id=\"outer\">\n");
      out.write("        <div id=\"wrapper\">\n");
      out.write("            <div id=\"body-bot\">\n");
      out.write("                <div id=\"body-top\">\n");
      out.write("                    <div id=\"logo\">\n");
      out.write("                        <h1>Vehicle Verification</h1>\n");
      out.write("                        <p>Check Your Vechicle's Details</p>\n");
      out.write("                    </div>\n");
      out.write("                    <div id=\"nav\">\n");
      out.write("                        <ul>\n");
      out.write("                            <li><a href=\"index.html\">Home</a></li>\n");
      out.write("                            <li><a href=\"login.html\">Special Login</a></li>\n");
      out.write("                            <li><a href=\"contactus.html\">Contact Us</a></li>\n");
      out.write("                        </ul>\n");
      out.write("          \n");
      out.write("                    <div class=\"clear\"> </div>\n");
      out.write("                </div>\n");
      out.write("                                <br/><br/><br/>\n");
      out.write("                                <center><h2>Welcome to Vehicle Verification Portal</h2></center>\n");
      out.write("                                <br/><br/>\n");
      out.write("\n");
      out.write("                                <div class=\"region region-content\">\n");
      out.write("                                    <section id=\"block-system-main\" class=\"block block-system clearfix\">\n");
      out.write("                                        <div id=\"node-6\" class=\"node node-page clearfix\" typeof=\"foaf:Document\">\n");
      out.write("                                            <div class=\"ent-main-div\">\n");
      out.write("                                            <center>\n");
      out.write("                                            <center><h3>Vehicle Details</h3></center>\n");
      out.write("                                            \n");
      out.write("        ");

            connect c = new connect();
            c.getVal("lname", request);

            ResultSet details = c.getDetails();
            if (details.next()) 
            {
               
      out.write("\n");
      out.write("                                            \n");
      out.write("                                            <br><br>\n");
      out.write("                                              <table>\n");
      out.write("                                                  <tbody>\n");
      out.write("                                                <tr>\n");
      out.write("                                                    <td>Registration Number:</td>\n");
      out.write("                                                    <td>");
      out.print( details.getString("registration_number"));
      out.write("</td>\n");
      out.write("                                                </tr>\n");
      out.write("                                                <tr>\n");
      out.write("                                                    <td>Chassis Number:</td>\n");
      out.write("                                                    <td>");
      out.print( details.getString("chassis_number") );
      out.write("</td>\n");
      out.write("                                                </tr>\n");
      out.write("                                                <tr>\n");
      out.write("                                                    <td>Engine Number:</td>\n");
      out.write("                                                    <td>");
      out.print( details.getString("engine_number") );
      out.write("</td>\n");
      out.write("                                                </tr>\n");
      out.write("                                                <tr>\n");
      out.write("                                                    <td>Make Name:</td>\n");
      out.write("                                                    <td>");
      out.print( details.getString("make") );
      out.write("</td>\n");
      out.write("                                                </tr>\n");
      out.write("                                                <tr>\n");
      out.write("                                                    <td>Model:</td>\n");
      out.write("                                                    <td>");
      out.print( details.getString("model") );
      out.write("</td>\n");
      out.write("                                                </tr>\n");
      out.write("                                                <tr>\n");
      out.write("                                                    <td>Vehicle Price:</td>\n");
      out.write("                                                    <td>");
      out.print( details.getString("vehicle_price") );
      out.write("</td>\n");
      out.write("                                                </tr>\n");
      out.write("                                                <tr>\n");
      out.write("                                                    <td>Color:</td>\n");
      out.write("                                                    <td>");
      out.print( details.getString("color") );
      out.write("</td>\n");
      out.write("                                                </tr>\n");
      out.write("                                               \n");
      out.write("                                                  </tbody>\n");
      out.write("                                              </table>\n");
      out.write("                                        <br><br>\n");
      out.write("                                        <center><h3>Owner Details</h3></center>\n");
      out.write("                                        <br><br>\n");
      out.write("                                            <table>\n");
      out.write("                                                <tr>\n");
      out.write("                                                    <td>Owner name:</td>\n");
      out.write("                                                    <td>");
      out.print( details.getString("owner_name") );
      out.write("</td>\n");
      out.write("                                                </tr>\n");
      out.write("                                                <tr>\n");
      out.write("                                                    <td>Father Name:</td>\n");
      out.write("                                                    <td>");
      out.print( details.getString("owner_father") );
      out.write("</td>\n");
      out.write("                                                </tr>\n");
      out.write("                                                <tr>\n");
      out.write("                                                    <td>Owner City:</td>\n");
      out.write("                                                    <td>");
      out.print( details.getString("owner_city") );
      out.write("</td>\n");
      out.write("                                                </tr>\n");
      out.write("                                              </table>  \n");
      out.write("                                                \n");

    }
    else
    {

      out.write(" \n");
      out.write("                                <br><br>\n");
      out.write("                                <h5>No Record Found</h5>\n");
      out.write("\n");

    }

      out.write("\n");
      out.write("                                                \n");
      out.write("                                            </center>\n");
      out.write("                                            </div>\n");
      out.write("                                        </div>\n");
      out.write("                                    </section>\n");
      out.write("                                </div>\n");
      out.write("                </div>\n");
      out.write("            </div>\n");
      out.write("        </div>\n");
      out.write("    </div>\n");
      out.write("</body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
