package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.sql.*;

public final class admin_002dhome_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {


            public class connect
            {
                String url = "jdbc:mysql://localhost/motor_database";			
                String rn = "root";
                String ps = "";
                String sql_query_insert = "select * from vehicle_registeration";
                
                Connection con = null;
                PreparedStatement psi = null;
                ResultSet rs = null;
                
                public connect()
                {
                    try
                    {
                        con=DriverManager.getConnection(url,rn,ps);
                        //Statement s = con.createStatement();			
                        psi = con.prepareStatement(sql_query_insert,ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);                    
                    }
                    catch(SQLException se)
                    {
                                
                    }
                }
                public ResultSet getdetails()
                {
                    try
                    {
                       rs = psi.executeQuery(); 
                    }
                    catch(SQLException se)
                    {
                                
                    }

                return rs;
                }    
            }
        
  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write('\n');
 Class.forName("com.mysql.jdbc.Driver");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<head>\n");
      out.write("      <meta charset=\"utf-8\" />\n");
      out.write("    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" />\n");
      out.write("    <title>Admin</title>\n");
      out.write("\n");
      out.write("    <link href=\"adminstyle.css\" rel=\"stylesheet\" />\n");
      out.write("    \n");
      out.write("    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />\n");
      out.write("</head>\n");
      out.write("<body>\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("        ");

            connect c = new connect();
            ResultSet details = c.getdetails();
        
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("    <div id=\"wrapper\">\n");
      out.write("        <nav class=\"navbar navbar-default navbar-cls-top \" role=\"navigation\" style=\"margin-bottom: 0\">\n");
      out.write("            <div class=\"navbar-header\">\n");
      out.write("                <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\".sidebar-collapse\">\n");
      out.write("                    <span class=\"icon-bar\"></span>\n");
      out.write("                    <span class=\"icon-bar\"></span>\n");
      out.write("                    <span class=\"icon-bar\"></span>\n");
      out.write("                </button>\n");
      out.write("                <a class=\"navbar-brand\" href=\"admin-home.jsp\">Registeration Admin</a> \n");
      out.write("            </div>\n");
      out.write("  <div style=\"color: white;\n");
      out.write("padding: 15px 50px 5px 50px;\n");
      out.write("float: right;\n");
      out.write("font-size: 16px;\"><a href=\"index.html\" class=\"btn btn-danger square-btn-adjust\">Logout</a> </div>\n");
      out.write("        </nav>   \n");
      out.write("\n");
      out.write("                <nav class=\"navbar-default navbar-side\" role=\"navigation\">\n");
      out.write("            <div class=\"sidebar-collapse\">\n");
      out.write("                <ul class=\"nav\" id=\"main-menu\">\n");
      out.write("\t\t\t\t<li class=\"text-center\">\n");
      out.write("                    <img src=\"images/find_user.png\" class=\"user-image img-responsive\"/>\n");
      out.write("\t\t\t\t\t</li>\n");
      out.write("\t\t\t\t\n");
      out.write("\t\t\t\t\t\n");
      out.write("                    <li>\n");
      out.write("                        <a class=\"active-menu\"  href=\"admin-home.jsp\"><i class=\"fa fa-dashboard fa-3x\"></i> Home</a>\n");
      out.write("                    </li>\n");
      out.write("                     <li>\n");
      out.write("                        <a  href=\"new-reg.html\"><i class=\"fa fa-desktop fa-3x\"></i> New Registeration</a>\n");
      out.write("                    </li>\n");
      out.write("                    <li>\n");
      out.write("                        <a  href=\"change-own.html\"><i class=\"fa fa-qrcode fa-3x\"></i> Change Ownership</a>\n");
      out.write("                    </li>\n");
      out.write("\t\t\t\t\t\t   <li  >\n");
      out.write("                        <a   href=\"update-eng-num.html\"><i class=\"fa fa-bar-chart-o fa-3x\"></i> Update Engine Number</a>\n");
      out.write("                    </li>\t\n");
      out.write("                      <li  >\n");
      out.write("                        <a  href=\"del-reg.html\"><i class=\"fa fa-table fa-3x\"></i> Delete Registeration</a>\n");
      out.write("                    </li>\n");
      out.write("                    \n");
      out.write("                </ul>\n");
      out.write("               \n");
      out.write("            </div>\n");
      out.write("            \n");
      out.write("        </nav>  \n");
      out.write("        <!-- /. NAV SIDE  -->\n");
      out.write("        <div id=\"page-wrapper\" >\n");
      out.write("           <div id=\"page-inner\">\n");
      out.write("                <div class=\"row\">\n");
      out.write("                    <div class=\"col-md-12\">\n");
      out.write("                     <h2>Admin Dashboard</h2>   \n");
      out.write("                    </div>\n");
      out.write("                </div>\n");
      out.write("                 <!-- /. ROW  -->\n");
      out.write("                 <hr />\n");
      out.write("               \n");
      out.write("            <div class=\"row\">\n");
      out.write("                <div class=\"col-md-12\">\n");
      out.write("                    <!-- Advanced Tables -->\n");
      out.write("                    <div class=\"panel panel-default\">\n");
      out.write("                        <div class=\"panel-heading\">\n");
      out.write("                             Registered Vehicles\n");
      out.write("                        </div>\n");
      out.write("                        <div class=\"panel-body\">\n");
      out.write("                            <div class=\"table-responsive\">\n");
      out.write("                                <table class=\"table table-striped table-bordered table-hover\" id=\"dataTables-example\">\n");
      out.write("                                    <thead>\n");
      out.write("                                        <tr>\n");
      out.write("                                            <th>Registration Number</th>\n");
      out.write("                                            <th>Owner Name</th>\n");
      out.write("                                            <th>Owner Father</th>\n");
      out.write("                                            <th>Owner City</th>\n");
      out.write("                                            <th>Make</th>\n");
      out.write("                                            <th>Model</th>\n");
      out.write("                                        </tr>\n");
      out.write("                                    </thead>\n");
      out.write("                                    <tbody>\n");
      out.write("                                       ");
 while(details.next()) { 
      out.write("\n");
      out.write("                \n");
      out.write("\t\t\t\t\t\t                <tr>\n");
      out.write("                    \t\t\t\t\t\t<td>");
      out.print( details.getString("registration_number") );
      out.write("</td>\n");
      out.write("                    \t\t\t\t\t\t<td>");
      out.print( details.getString("owner_name") );
      out.write("</td>\n");
      out.write("                                                                <td>");
      out.print( details.getString("owner_father") );
      out.write("</td>\n");
      out.write("                    \t\t\t\t\t\t<td>");
      out.print( details.getString("owner_city") );
      out.write("</td>\n");
      out.write("                    \t\t\t\t\t\t<td>");
      out.print( details.getString("make") );
      out.write("</td>\n");
      out.write("                    \t\t\t\t\t\t<td>");
      out.print( details.getString("model") );
      out.write("</td>                  \t\t\t\t\t\t\n");
      out.write("                \t\t\t\t\t\t</tr>\n");
      out.write("                \t\t\t\t\t\t");
 } 
      out.write("\n");
      out.write("                                    </tbody>\n");
      out.write("                                </table>\n");
      out.write("                            </div>\n");
      out.write("                            \n");
      out.write("                        </div>\n");
      out.write("                    </div>\n");
      out.write("                </div>\n");
      out.write("            </div>\n");
      out.write("        </div>        \n");
      out.write("  \n");
      out.write("    \t</div>\n");
      out.write("   \n");
      out.write("</body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
