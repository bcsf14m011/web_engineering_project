package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class index_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("<!DOCTYPE HTML>\n");
      out.write("<head>\n");
      out.write("<title>Vehicle Verification</title>\n");
      out.write("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"/>\n");
      out.write("<link rel=\"stylesheet\" href=\"homestyle.css\" type=\"text/css\" charset=\"utf-8\" />\n");
      out.write("</head>\n");
      out.write("<body>\n");
      out.write("\n");
      out.write("\n");
      out.write("\t<style>\n");
      out.write("\t\t.ent-main-div {\n");
      out.write("\t\t\twidth: 40%;\n");
      out.write("\t\t\tmargin: auto;\n");
      out.write("\t\t\tpadding: 30px;\n");
      out.write("\t\t\tbackground-color: #f5f5f5;\n");
      out.write("\t\t\tborder-radius: 10px;\n");
      out.write("\t\t}\n");
      out.write("\t\t.ent-info-div {\n");
      out.write("\t\t\tfont-size: 10px;\n");
      out.write("\t\t\tmargin: auto;\n");
      out.write("\t\t\twidth: 50%;\n");
      out.write("\t\t}\n");
      out.write("\t\t.ent-textbox-div {\n");
      out.write("\t\t\tpadding: 20px;\n");
      out.write("\t\t}\n");
      out.write("\t\t.ent-textbox-div span {\n");
      out.write("\t\t\tfont-weight: bold;\n");
      out.write("\t\t}\n");
      out.write("\t\t.ent-textbox-div input {\n");
      out.write("\t\t\twidth: 100%;\n");
      out.write("\t\t}\n");
      out.write("\t\t.ent-button-div {\n");
      out.write("\t\t\tpadding-right: 20px;\n");
      out.write("\t\t\ttext-align: right;\n");
      out.write("\t\t}\n");
      out.write("\t\t@media screen and (max-width: 767px){\n");
      out.write("\t\t\t.ent-main-div {\n");
      out.write("\t\t\t\twidth: 100%;\n");
      out.write("\t\t\t\tmargin: auto;\n");
      out.write("\t\t\t\tpadding: 15px;\n");
      out.write("\t\t\t\tbackground-color: #f5f5f5;\n");
      out.write("\t\t\t\tborder-radius: 10px;\n");
      out.write("\t\t\t}\n");
      out.write("\t\t\t.ent-info-div {\n");
      out.write("\t\t\t\tfont-size: 10px;\n");
      out.write("\t\t\t\tmargin: auto;\n");
      out.write("\t\t\t\twidth: 70%;\n");
      out.write("\t\t\t\ttext-align: center;\n");
      out.write("\t\t\t}\n");
      out.write("\t\t}\n");
      out.write("\t</style>\n");
      out.write("\n");
      out.write("\n");
      out.write("\t<div id=\"outer\">\n");
      out.write("            <div id=\"wrapper\">\n");
      out.write("    \t\t<div id=\"body-bot\">\n");
      out.write("                    <div id=\"body-top\">\n");
      out.write("        \t\t<div id=\"logo\">\n");
      out.write("                            <h1>Vehicle Verification</h1>\n");
      out.write("                            <p>Check Your Vechicle's Details</p>\n");
      out.write("        \t\t</div>\n");
      out.write("        \t\t<div id=\"nav\">\n");
      out.write("                            <ul>\n");
      out.write("                                <li><a href=\"index.html\">Home</a></li>\n");
      out.write("                                <li><a href=\"login.jsp\">Special Login</a></li>\n");
      out.write("                                <li><a href=\"contactus.html\">Contact Us</a></li>\n");
      out.write("                            </ul>\n");
      out.write("          \n");
      out.write("                            <div class=\"clear\"> </div>\n");
      out.write("                        </div>\n");
      out.write("                        \n");
      out.write("                        <br/><br/><br/>\n");
      out.write("                        <center><h2>Welcome to Vehicle Verification Portal</h2></center>\n");
      out.write("                         <br/><br/>\n");
      out.write("\n");
      out.write("                        <div class=\"region region-content\">\n");
      out.write("                            <section id=\"block-system-main\" class=\"block block-system clearfix\">\n");
      out.write("                                <div id=\"node-6\" class=\"node node-page clearfix\" typeof=\"foaf:Document\">\n");
      out.write("                                    <div class=\"ent-main-div\">\n");
      out.write("                                        <form id=\"searchf\" method=\"post\" action=\"ajax.jsp\">\n");
      out.write("                                            <div class=\"ent-info-div\">\n");
      out.write("                                                Please Enter in Format as below<br><br>\n");
      out.write("                                                ABC 000 ......................LXZ 123<br>\n");
      out.write("                                                ABC 0000 ....................LXZ 0123<br>\n");
      out.write("                                                ABC 0000 ....................LXZ 1111<br>\n");
      out.write("                                            </div>\n");
      out.write("                                            \n");
      out.write("                                            <div class=\"ent-textbox-div\">\n");
      out.write("                                                <span>Vehicle Number:</span>\n");
      out.write("                                                <input type=\"text\" id=\"vid\">\n");
      out.write("                                            </div>\n");
      out.write("                                \t\n");
      out.write("                                            <div class=\"ent-button-div\">\n");
      out.write("                                                <button type=\"button\" onclick=\"loadXMLDoc()\">Search</button>\n");
      out.write("                                            </div>\n");
      out.write("                                        </form>\n");
      out.write("                                    </div>\n");
      out.write("                                </div>\n");
      out.write("                            </section>\n");
      out.write("                            <br/>\n");
      out.write("                            <div id=\"result\"></div>\n");
      out.write("            \t\t\n");
      out.write("                        </div>\n");
      out.write("                    </div>\n");
      out.write("              \t</div>\n");
      out.write("             </div>\n");
      out.write("        </div>\n");
      out.write("\n");
      out.write("<script>\n");
      out.write("function loadXMLDoc() {\n");
      out.write("  \n");
      out.write("    var xhttp = new XMLHttpRequest();\n");
      out.write("  \n");
      out.write("    var url = \"ajax.jsp\";\n");
      out.write("    \n");
      out.write("    var vn = document.getElementById(\"vid\").value;    \n");
      out.write("    var paramn = \"vhlno=\";\n");
      out.write("    var param = paramn.concat(vn);  \n");
      out.write("\n");
      out.write("    xhttp.onreadystatechange = function() {\n");
      out.write("    if (this.readyState === 4) {\n");
      out.write("      document.getElementById(\"result\").innerHTML =\n");
      out.write("      this.responseText;\n");
      out.write("      document.getElementById(\"searchf\").reset();\n");
      out.write("    }\n");
      out.write("  };\n");
      out.write("  xhttp.open(\"GET\",url+\"?\"+param, true);\n");
      out.write("  xhttp.send();\n");
      out.write("}\n");
      out.write("</script>\n");
      out.write("</body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
