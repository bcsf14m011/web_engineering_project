package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class new_002dreg_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");


    if(!(null == session.getAttribute("type")))
    {
        String type = null;
        type = (String)session.getAttribute("type");

        if(type.equals("Admin"))
        {

      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html xmlns=\"http://www.w3.org/1999/xhtml\">\n");
      out.write("<head>\n");
      out.write("      <meta charset=\"utf-8\" />\n");
      out.write("    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" />\n");
      out.write("    <title>Admin</title>\n");
      out.write("\t <link href=\"adminstyle.css\" rel=\"stylesheet\" />\n");
      out.write("   <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />\n");
      out.write("</head>\n");
      out.write("<body>\n");
      out.write("    <div id=\"wrapper\">\n");
      out.write("        <nav class=\"navbar navbar-default navbar-cls-top \" role=\"navigation\" style=\"margin-bottom: 0\">\n");
      out.write("            <div class=\"navbar-header\">\n");
      out.write("                <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\".sidebar-collapse\">\n");
      out.write("                    <span class=\"icon-bar\"></span>\n");
      out.write("                    <span class=\"icon-bar\"></span>\n");
      out.write("                    <span class=\"icon-bar\"></span>\n");
      out.write("                </button>\n");
      out.write("                <a class=\"navbar-brand\" href=\"admin-home.jsp\">Registeration Admin</a> \n");
      out.write("            </div>\n");
      out.write("  <div style=\"color: white;\n");
      out.write("padding: 15px 50px 5px 50px;\n");
      out.write("float: right;\n");
      out.write("font-size: 16px;\"><a href=\"endsession\" class=\"btn btn-danger square-btn-adjust\">Logout</a> </div>\n");
      out.write("        </nav>   \n");
      out.write("\n");
      out.write("                <nav class=\"navbar-default navbar-side\" role=\"navigation\">\n");
      out.write("            <div class=\"sidebar-collapse\">\n");
      out.write("                <ul class=\"nav\" id=\"main-menu\">\n");
      out.write("\t\t\t\t<li class=\"text-center\">\n");
      out.write("                    <img src=\"images/find_user.png\" class=\"user-image img-responsive\"/>\n");
      out.write("\t\t\t\t\t</li>\n");
      out.write("\t\t\t\t\n");
      out.write("\t\t\t\t\t\n");
      out.write("                    <li>\n");
      out.write("                        <a href=\"admin-home.jsp\"><i class=\"fa fa-dashboard fa-3x\"></i> Home</a>\n");
      out.write("                    </li>\n");
      out.write("                     <li>\n");
      out.write("                        <a class=\"active-menu\"  href=\"new-reg.jsp\"><i class=\"fa fa-desktop fa-3x\"></i> New Registeration</a>\n");
      out.write("                    </li>\n");
      out.write("                    <li>\n");
      out.write("                        <a  href=\"change-own.jsp\"><i class=\"fa fa-qrcode fa-3x\"></i> Change Ownership</a>\n");
      out.write("                    </li>\n");
      out.write("\t\t\t\t\t\t   <li  >\n");
      out.write("                        <a   href=\"update-eng-num.jsp\"><i class=\"fa fa-bar-chart-o fa-3x\"></i> Update Engine Number</a>\n");
      out.write("                    </li>\t\n");
      out.write("                      <li  >\n");
      out.write("                        <a  href=\"del-reg.jsp\"><i class=\"fa fa-table fa-3x\"></i> Delete Registeration</a>\n");
      out.write("                    </li>\n");
      out.write("                    \n");
      out.write("                </ul>\n");
      out.write("               \n");
      out.write("            </div>\n");
      out.write("            \n");
      out.write("        </nav>  \n");
      out.write("        <div id=\"page-wrapper\" >\n");
      out.write("            <div id=\"page-inner\">\n");
      out.write("                <div class=\"row\">\n");
      out.write("                <div class=\"col-md-12\">\n");
      out.write("\n");
      out.write("                    <div class=\"panel panel-default\">\n");
      out.write("                        <div class=\"panel-heading\">\n");
      out.write("                            New Registration\n");
      out.write("                        </div>\n");
      out.write("                        <div class=\"panel-body\">\n");
      out.write("                            <div class=\"row\">\n");
      out.write("                                <div class=\"col-md-6\">\n");
      out.write("                                    <h3>Registeration Details</h3>\n");
      out.write("                                    <br/><br/>\n");
      out.write("                                    <form role=\"form\" action=\"newreg\" method=\"post\">\n");
      out.write("                                        <div class=\"form-group\">\n");
      out.write("                                            <label>Registration Number</label>\n");
      out.write("                                            <input name=\"regnum\" class=\"form-control\" placeholder=\"LEA-17-333\" />\n");
      out.write("                                        </div>\n");
      out.write("                                        <div class=\"form-group\">\n");
      out.write("                                            <label>Chassis Number</label>\n");
      out.write("                                            <input name=\"cnum\" class=\"form-control\" placeholder=\"LJCPCBLCX11000237\" />\n");
      out.write("                                        </div>\n");
      out.write("                                        <div class=\"form-group\">\n");
      out.write("                                            <label>Engine Number</label>\n");
      out.write("                                            <input name=\"enum\" class=\"form-control\" placeholder=\"321PCBLCX11CPL237\" />\n");
      out.write("                                        </div>\n");
      out.write("                                        <div class=\"form-group\">\n");
      out.write("                                            <label>Make</label>\n");
      out.write("                                            <select name=\"make\" class=\"form-control\">\n");
      out.write("                                                    <option value=\"Toyota\">Toyota</option>\n");
      out.write("                                                    <option value=\"Honda\">Honda</option>\n");
      out.write("                                                    <option value=\"Suzuki\">Suzuki</option>\n");
      out.write("                                                    <option value=\"Daihatsu\">Daihatsu</option>\n");
      out.write("                                                    <option value=\"Porsche\">Porsche</option>\n");
      out.write("                                                    <option value=\"Ford\">Ford</option>\n");
      out.write("                                                    <option value=\"Bentley\">Bentley</option>\n");
      out.write("                                                    <option value=\"Other\">Other</option>\n");
      out.write("                                                </select>\n");
      out.write("                                        </div>\n");
      out.write("                                        <div class=\"form-group\">\n");
      out.write("                                            <label>Model</label>\n");
      out.write("                                            <input name=\"model\" class=\"form-control\" placeholder=\"1992 Corolla\" />\n");
      out.write("                                        </div>\n");
      out.write("                                        <div class=\"form-group\">\n");
      out.write("                                            <label>Color</label>\n");
      out.write("                                            <select name=\"color\" class=\"form-control\">\n");
      out.write("                                                    <option value=\"White\">White</option>\n");
      out.write("                                                    <option value=\"Black\">Black</option>\n");
      out.write("                                                    <option value=\"Silver\">Silver</option>\n");
      out.write("                                                    <option value=\"Charcol\">Charcol</option>\n");
      out.write("                                                    <option value=\"Gold\">Gold</option>\n");
      out.write("                                                    <option value=\"Blue\">Blue</option>\n");
      out.write("                                                    <option value=\"Red\">Red</option>\n");
      out.write("                                                    <option value=\"Green\">Green</option>\n");
      out.write("                                                    <option value=\"Other\">Other</option>\n");
      out.write("                                                </select>\n");
      out.write("                                        </div>\n");
      out.write("                                        <div class=\"form-group\">\n");
      out.write("                                            <label>Vehicle Price</label>\n");
      out.write("                                            <input type=\"number\" name=\"price\" class=\"form-control\" placeholder=\"3800000\" />\n");
      out.write("                                        </div>\n");
      out.write("                                        <div class=\"form-group\">\n");
      out.write("                                            <label>Owner Name</label>\n");
      out.write("                                            <input name=\"ownname\" class=\"form-control\" placeholder=\"Ahsan Malik\" />\n");
      out.write("                                        </div>\n");
      out.write("                                         <div class=\"form-group\">\n");
      out.write("                                            <label>Owner Father</label>\n");
      out.write("                                            <input name=\"ownf\" class=\"form-control\" placeholder=\"Talha Jahangir\" />\n");
      out.write("                                        </div>\n");
      out.write("                                        <div class=\"form-group\">\n");
      out.write("                                            <label>Owner City</label>\n");
      out.write("                                            <select name=\"ownc\" class=\"form-control\">\n");
      out.write("                                                    <option value=\"Lahore\">Lahore</option>\n");
      out.write("                                                    <option value=\"Islamabad\">Islamabad</option>\n");
      out.write("                                                    <option value=\"Faisalabad\">Faisalabad</option>\n");
      out.write("                                                    <option value=\"Multan\">Multan</option>\n");
      out.write("                                                    <option value=\"Bahawalpur\">Bahawalpur</option>\n");
      out.write("                                                    <option value=\"Mianwali\">Mianwali</option>\n");
      out.write("                                                    <option value=\"Sialkot\">Sialkot</option>\n");
      out.write("                                                    <option value=\"DG Khan\">Dera Ghazi Khan</option>\n");
      out.write("                                                </select>\n");
      out.write("                                        </div>\n");
      out.write("                                        <br/><br/>\n");
      out.write("                                        <button onclick=\"{return confirmComplete();}\" type=\"submit\" class=\"btn btn-success pull-right btn-lg\">Register Vehicle</button>\n");
      out.write("                                    </form>\n");
      out.write("                                    </div>\n");
      out.write("                                </div>\n");
      out.write("                            </div>\n");
      out.write("                        </div>\n");
      out.write("                    </div>\n");
      out.write("                </div>\n");
      out.write("            </div>\n");
      out.write("        </div>\n");
      out.write("    </div>\n");
      out.write("    \n");
      out.write("    <script type=\"text/javascript\">\n");
      out.write("            function confirmComplete() \n");
      out.write("            {\n");
      out.write("                \n");
      out.write("                var answer=confirm(\"Are you sure you want to continue\");\n");
      out.write("                \n");
      out.write("                if (answer === true)\n");
      out.write("                {\n");
      out.write("                    //if(validate())\n");
      out.write("//                    {\n");
      out.write("                        alert(\"Data Submitted Sucessfully\");\n");
      out.write("                        return true;\n");
      out.write("  //                  }\n");
      out.write("                }\n");
      out.write("                else\n");
      out.write("                {\n");
      out.write("                    alert(\"Data Not Submitted\");\n");
      out.write("                    return false;\n");
      out.write("                }\n");
      out.write("            }\n");
      out.write("        </script>\n");
      out.write("</body>\n");
      out.write("</html>\n");
      out.write("\n");

        }
        else
        {
            response.sendRedirect("login.jsp");
        }
    }
    else
    {
        response.sendRedirect("login.jsp");
    }

    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
