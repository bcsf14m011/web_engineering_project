<%
    if(!(null == session.getAttribute("type")))
    {
        String type = null;
        type = (String)session.getAttribute("type");

        if(type.equals("Admin"))
        {
%>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
      <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Admin</title>
    <link href="adminstyle.css" rel="stylesheet" />
   <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
</head>
<body>
    <div id="wrapper">
        <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="admin-home.jsp">Registeration Admin</a> 
            </div>
  <div style="color: white;
padding: 15px 50px 5px 50px;
float: right;
font-size: 16px;"><a href="endsession" class="btn btn-danger square-btn-adjust">Logout</a> </div>
        </nav>   
                <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">
                <li class="text-center">
                    <img src="images/find_user.png" class="user-image img-responsive"/>
                    </li>
                
                    
                    <li>
                        <a  href="admin-home.jsp"><i class="fa fa-dashboard fa-3x"></i> Home</a>
                    </li>
                     <li>
                        <a  href="new-reg.jsp"><i class="fa fa-desktop fa-3x"></i> New Registeration</a>
                    </li>
                    <li>
                        <a   href="change-own.jsp"><i class="fa fa-qrcode fa-3x"></i> Change Ownership</a>
                    </li>
                           <li  >
                        <a   href="update-eng-num.jsp"><i class="fa fa-bar-chart-o fa-3x"></i> Update Engine Number</a>
                    </li>   
                      <li  >
                        <a class="active-menu" href="del-reg.jsp"><i class="fa fa-table fa-3x"></i> Delete Registeration</a>
                    </li>
                   
                </ul>
               
            </div>
            
        </nav>  

        <div id="page-wrapper" >
            <div id="page-inner">
                <div class="row">
                </div>

                 <hr />
               <div class="row">
                <div class="col-md-12">
                    <!-- Form Elements -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Delete Vehicle Record
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <h3>Vehicle Details</h3>
                                    <br/><br/>
                                    <form role="form" action="delreg" method="post">
                                        <div class="form-group">
                                            <label>Registration Number</label>
                                            <input name="regnum" class="form-control" placeholder="LEA-17-333" />
                                        </div>
                                        <br/><br/>
                                        <input type="submit" class="btn btn-success pull-right btn-lg" name="submitbutton" onclick="{return confirmComplete();}" value="Delete Vehicle">
                                    </form>
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    
        <script type="text/javascript">
            function confirmComplete() 
            {
                
                var answer=confirm("Are you sure you want to continue");
                
                if (answer === true)
                {
                    if(validate())
                    {
                        alert("Data Submitted Sucessfully");
                        return true;
                    }
                }
                else
                {
                    alert("Data Not Submitted");
                    return false;
                }
            }
            function validate()
            {
                var name=document.inputform.name.value;  
                if (name === null || name === "")
                {  
                    alert("Name can't be blank");  
                    return false;  
                }
                
            }
 </script>

    
</body>
</html>

<%
        }
        else
        {
            response.sendRedirect("login.jsp");
        }
    }
    else
    {
        response.sendRedirect("login.jsp");
    }
%>