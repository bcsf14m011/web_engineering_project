<!DOCTYPE HTML>
<head>
<title>Vehicle Verification</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<link rel="stylesheet" href="homestyle.css" type="text/css" charset="utf-8" />
</head>
<body>


	<style>
		.ent-main-div {
			width: 40%;
			margin: auto;
			padding: 30px;
			background-color: #f5f5f5;
			border-radius: 10px;
		}
		.ent-info-div {
			font-size: 10px;
			margin: auto;
			width: 50%;
		}
		.ent-textbox-div {
			padding: 20px;
		}
		.ent-textbox-div span {
			font-weight: bold;
		}
		.ent-textbox-div input {
			width: 100%;
		}
		.ent-button-div {
			padding-right: 20px;
			text-align: right;
		}
		@media screen and (max-width: 767px){
			.ent-main-div {
				width: 100%;
				margin: auto;
				padding: 15px;
				background-color: #f5f5f5;
				border-radius: 10px;
			}
			.ent-info-div {
				font-size: 10px;
				margin: auto;
				width: 70%;
				text-align: center;
			}
		}
	</style>


	<div id="outer">
            <div id="wrapper">
    		<div id="body-bot">
                    <div id="body-top">
        		<div id="logo">
                            <h1>Vehicle Verification</h1>
                            <p>Check Your Vechicle's Details</p>
        		</div>
        		<div id="nav">
                            <ul>
                                <li><a href="index.jsp">Home</a></li>
                                <li><a href="login.jsp">Special Login</a></li>
                                <li><a href="contactus.html">Contact Us</a></li>
                            </ul>
          
                            <div class="clear"> </div>
                        </div>
                        
                        <br/><br/><br/>
                        <center><h2>Welcome to Vehicle Verification Portal</h2></center>
                         <br/><br/>

                        <div class="region region-content">
                            <section id="block-system-main" class="block block-system clearfix">
                                <div id="node-6" class="node node-page clearfix" typeof="foaf:Document">
                                    <div class="ent-main-div">
                                        <form id="searchf" method="post" action="ajax.jsp">
                                            <div class="ent-info-div">
                                                Please Enter in Format as below<br><br>
                                                ABC 000 ......................LXZ 123<br>
                                                ABC 0000 ....................LXZ 0123<br>
                                                ABC 0000 ....................LXZ 1111<br>
                                            </div>
                                            
                                            <div class="ent-textbox-div">
                                                <span>Vehicle Number:</span>
                                                <input type="text" id="vid">
                                            </div>
                                	
                                            <div class="ent-button-div">
                                                <button type="button" onclick="loadXMLDoc()">Search</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </section>
                            <br/>
                            <div id="result"></div>
            		
                        </div>
                    </div>
              	</div>
             </div>
        </div>

<script>
function loadXMLDoc() {
  
    var xhttp = new XMLHttpRequest();
  
    var url = "ajax.jsp";
    
    var vn = document.getElementById("vid").value;    
    var paramn = "vhlno=";
    var param = paramn.concat(vn);  

    xhttp.onreadystatechange = function() {
    if (this.readyState === 4) {
      document.getElementById("result").innerHTML =
      this.responseText;
      document.getElementById("searchf").reset();
    }
  };
  xhttp.open("GET",url+"?"+param, true);
  xhttp.send();
}
</script>
</body>
</html>
