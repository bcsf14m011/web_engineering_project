<%@ page import="java.io.*,java.util.*" %>

<%
    
    
    if(session.isNew() || (null == session.getAttribute("type")))
    {
%>

<!DOCTYPE html>
<html >
<head>
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" href="loginstyle.css" type="text/css" charset="utf-8" />
    <title>Special login</title>


</head>

<body>

<form action="login" method="post">
    <header>Special Login</header>
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <label >User Name <span>*</span></label>
    <input name="uname" required/>
    <label>Password<span>*</span></label>
    <input type="password" name="password" required/>
    <label>Login Type<span>*</span></label>
    <input name="type" required>
    <button type="submit">Login</button>
</form>


</body>
</html>
<%
    }
    else
    {   
        String type = null;
        type = (String)session.getAttribute("type");
    
        if(type.equals("Dealer"))
        {
            response.sendRedirect("dealer.jsp");

        }
        if(type.equals("Admin"))
        {
            response.sendRedirect("admin-home.jsp");
        }
}
%>